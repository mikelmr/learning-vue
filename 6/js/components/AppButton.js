export default {
    template: `
        <button 
            :class="{
                'primary' : type === 'primary',
                'secondary' : type === 'secondary',
                'muted' : type === 'muted',
                'is-loading' : processing
            }"
            :disabled="processing"
        >
            <slot />
        </button>
    `,

    props: {
        type: {
            type: String,   
            default: 'primary',
        },
        processing: {
            type: Boolean,
            default: false,
        }
    },
};