export default {
    template: `
    <li>
        <label>
            {{ assignment.text }}
            <input type="checkbox" v-model="assignment.completed">
        </label>
    </li>
    `,

    props: {
        assignment: Object,
    },
};