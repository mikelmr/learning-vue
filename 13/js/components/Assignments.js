import AssignmentList from "./AssignmentList.js";
import AssignmentCreate from "./AssignmentCreate.js";

export default {
    components: { AssignmentList, AssignmentCreate },
    template: `
        <assignment-list :assignments="filters.inProgress" title="In Progress"></assignment-list>
        <assignment-list :assignments="filters.completed" title="Completed"></assignment-list>
        <assignment-create @add="add"></assignment-create>
    `,

    data() {
        return {
            assignments: [

            ],
        };
    },
    computed: {
        filters() {
            return {
                inProgress: this.assignments.filter(assignment => ! assignment.completed),
                completed: this.assignments.filter(assignment => assignment.completed),
            };
        }
    },

    created() {
        fetch('http://localhost:3001/assignments')
            .then(response => response.json())
            .then(data => this.assignments = data);
    },

    methods: {
        add(text) {
            this.assignments.push({
                id: this.assignments.length + 1,
                text: text,
                completed: false,
            });
        }
    }
    
};