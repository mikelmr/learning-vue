import AssignmentList from "./AssignmentList.js";

export default {
    components: { AssignmentList },
    template: `
        <assignment-list :assignments="filters.inProgress" title="In Progress"></assignment-list>
        <assignment-list :assignments="filters.completed" title="Completed"></assignment-list>
        <form @submit.prevent="Add">
            <input v-model="newAssignment" placeholder="New assignment">
            <button>Add</button>
        </form>
    `,

    data() {
        return {
            assignments: [
                { id: 1, text: 'Prueba1', completed: false },
                { id: 2, text: 'Prueba2', completed: false },
                { id: 3, text: 'Prueba3', completed: false },
            ],

            newAssignment: '',
        };
    },
    computed: {
        filters() {
            return {
                inProgress: this.assignments.filter(assignment => ! assignment.completed),
                completed: this.assignments.filter(assignment => assignment.completed),
            };
        }
    },

    methods: {
        Add() {
            this.assignments.push({
                id: this.assignments.length + 1,
                text: this.newAssignment,
                completed: false,
            });

            this.newAssignment = '';
        }
    }
    
};