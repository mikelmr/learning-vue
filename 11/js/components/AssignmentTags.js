export default {
    template: `
        <div>
            <button 
                v-for="tag in tags"
                @click="$emit('tagFilter', tag)"
                :class="{
                    'selected' : currentTag === tag
                }"
            >{{ tag }}</button>
        </div>
    `,

    props: {
        initialTags: Array,
        currentTag: String,
    },

    computed: {
        tags() {
            return ['all', ...new Set(this.initialTags)];
        }
    },
};