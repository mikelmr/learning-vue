import AssignmentList from "./AssignmentList.js";
import AssignmentCreate from "./AssignmentCreate.js";

export default {
    components: { AssignmentList, AssignmentCreate },
    template: `
        <assignment-list :assignments="filters.inProgress" title="In Progress"></assignment-list>
        <assignment-list :assignments="filters.completed" title="Completed"></assignment-list>
        <assignment-create @add="add"></assignment-create>
    `,

    data() {
        return {
            assignments: [
                { id: 1, text: 'Prueba1', completed: false, tag: 'Uno'},
                { id: 2, text: 'Prueba2', completed: false, tag: 'Dos'},
                { id: 3, text: 'Prueba3', completed: false, tag: 'Uno'},
            ],
        };
    },
    computed: {
        filters() {
            return {
                inProgress: this.assignments.filter(assignment => ! assignment.completed),
                completed: this.assignments.filter(assignment => assignment.completed),
            };
        }
    },

    methods: {
        add(text) {
            this.assignments.push({
                id: this.assignments.length + 1,
                text: text,
                completed: false,
            });
        }
    }
    
};