import { describe, it, expect, beforeEach } from 'vitest'

import { mount } from '@vue/test-utils'
import Counter from '../Counter.vue'

describe('Counter', () => {
  let wrapper

  beforeEach(() => {
    wrapper = mount(Counter)
  })

  it('defaults to a count of 0', () => {
    expect(wrapper.vm.count).toBe(0)
  })

  it('increments the count when the increment button is clicked', () => {
    expect(wrapper.vm.count).toBe(0)

    wrapper.find('.increment').trigger('click')

    expect(wrapper.vm.count).toBe(1)
  })

  it('decrements the count when the decrement button is clicked', () => {
    wrapper.setData({
      count: 5
    })

    wrapper.find('.decrement').trigger('click')

    expect(wrapper.vm.count).toBe(4)
  })

  it('can not decrement below 0', () => {
    expect(wrapper.vm.count).toBe(0)

    wrapper.find('.decrement').trigger('click')

    expect(wrapper.vm.count).toBe(0)
  })

  it('presents the current count', async () => {
    expect(wrapper.find('.count').html()).toContain(0)

    wrapper.find('.increment').trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.find('.count').text()).toContain(1)
  })
})
