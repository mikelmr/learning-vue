import { describe, it, expect, beforeEach } from 'vitest'

import { mount } from '@vue/test-utils'
import CouponCode from '../CouponCode.vue'

describe('CouponCode', () => {
  let wrapper

  beforeEach(() => {
    wrapper = mount(CouponCode)
  })

  it('accepts a coupon code', () => {
    let couponCodeInput = wrapper.find('input.coupon-code')

    expect(couponCodeInput.exists()).toBe(true)
  })

  it('validates a real coupon code', async () => {
    enterCouponCode('50OFF')
    await wrapper.vm.$nextTick()

    expect(wrapper.html()).toContain('Coupon Redeemed: 50% Off!')
  })

  it('validates a fake coupon code', async () => {
    enterCouponCode('NOTREAL')
    await wrapper.vm.$nextTick()

    expect(wrapper.html()).toContain('Invalid Coupon Code')
  })

  it('broadcasts the percentage discount when a valid coupon code is applied', async () => {
    enterCouponCode('50OFF')
    await wrapper.vm.$nextTick()

    expect(wrapper.emitted().applied).toBeTruthy()
    expect(wrapper.emitted().applied[0]).toEqual([50])
  })

  function enterCouponCode(code) {
    let couponCodeInput = wrapper.find('input.coupon-code')

    couponCodeInput.element.value = code
    couponCodeInput.trigger('input')
  }
})
