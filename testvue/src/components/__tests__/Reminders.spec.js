import { describe, it, expect, beforeEach } from 'vitest'

import { mount } from '@vue/test-utils'
import Reminders from '../Reminders.vue'

describe('Reminders', () => {
  let wrapper

  beforeEach(() => {
    wrapper = mount(Reminders)
  })

  it('hides the reminders list if there are none', () => {
    const ulElement = wrapper.find('ul')

    expect(ulElement.exists()).toBe(false)
  })

  it('can add reminders', async () => {
    addReminder('Go to the store')
    await wrapper.vm.$nextTick()

    expect(wrapper.find('ul').text()).toContain('Go to the store')
  })

  it('can add reminders', async () => {
    addReminder('Go to the store')
    addReminder('Finish screencast')
    await wrapper.vm.$nextTick()

    let removeButton = wrapper.find('ul > li:first-child .remove')
    removeButton.trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find('ul').text()).not.toContain('Go to the store')
    expect(wrapper.find('ul').text()).toContain('Finish screencast')
  })

  async function addReminder(body) {
    let newReminder = wrapper.find('.new-reminder')

    newReminder.element.value = body
    newReminder.trigger('input')

    wrapper.find('button').trigger('click')
  }
})
