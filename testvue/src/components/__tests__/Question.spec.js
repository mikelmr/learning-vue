import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest'

import { mount, flushPromises } from '@vue/test-utils'
import Question from '../Question.vue'
import axios from 'axios'

describe('Question', () => {
  let wrapper

  beforeEach(() => {
    wrapper = mount(Question, {
      propsData: {
        dataQuestion: {
          title: 'The title',
          body: 'The body'
        }
      }
    })
  })

  it('presents the title and the body', () => {
    see('The title')
    see('The body')
  })

  it('can be edited', async () => {
    let inputTitle = wrapper.find('input[name=title')
    expect(inputTitle.exists()).toBe(false)

    wrapper.find('#edit').trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.find('input[name=title').element.value).toBe('The title')
    expect(wrapper.find('textarea[name=body').element.value).toBe('The body')
  })

  it('hides the edit button during edit mode', async () => {
    wrapper.find('#edit').trigger('click')
    await wrapper.vm.$nextTick()

    let editButton = wrapper.find('#edit')
    expect(editButton.exists()).toBe(false)
  })

  it('updates the question after being edited', async () => {
    wrapper.find('#edit').trigger('click')
    await wrapper.vm.$nextTick()

    type('Changed title', 'input[name=title]')
    type('Changed body', 'textarea[name=body]')

    vi.spyOn(axios, 'post').mockImplementation(() => {
      return Promise.resolve({
        data: {
          title: 'Changed title',
          body: 'Changed body'
        }
      })
    })

    wrapper.find('#update').trigger('click')
    await wrapper.vm.$nextTick()

    await flushPromises()

    see('Changed title')
    see('Changed body')
    see('Your question has been updated.')
  })

  it('cancel the question being edited', async () => {
    wrapper.find('#edit').trigger('click')
    await wrapper.vm.$nextTick()

    type('Changed title', 'input[name=title]')

    wrapper.find('#cancel').trigger('click')
    await wrapper.vm.$nextTick()

    see('The title')
  })

  let see = (text, selector) => {
    let wrap = selector ? wrapper.find(selector) : wrapper

    expect(wrap.html()).toContain(text)
  }

  let type = (text, selector) => {
    let node = wrapper.find(selector)
    node.setValue(text)
    node.trigger('input')
  }
})
