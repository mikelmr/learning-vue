import Assignment from "./Assignment.js";

export default {
    components: { Assignment },
    template: `
        <section v-show="assignments.length">
            <h2>
                {{ title }} 
                <span>({{ assignments.length}})</span>
            </h2>
            <div>
                <button 
                    v-for="tag in tags"
                    @click="currentTag = tag"
                    :class="{
                        'selected' : currentTag === tag
                    }"
                >{{ tag }}</button>
            </div>
            <ul>
                <assignment
                    v-for="assignment in filteredAssignments"
                    :key="assignment.id"
                    :assignment="assignment"
                ></assignment>
            </ul>
        </section>
    `,

    props: {
        assignments: Array,
        title: {
            type: String,   
            default: 'List',
        },
    },

    data() {
        return {
            currentTag: 'all',
        };
    },

    computed: {
        filteredAssignments() {
            if (this.currentTag === 'all') {
                return this.assignments;
            }
            
            return this.assignments.filter(a => a.tag === this.currentTag);
        },
        tags() {
            return ['all', ...new Set(this.assignments.map(a => a.tag))];
        }
    }
};